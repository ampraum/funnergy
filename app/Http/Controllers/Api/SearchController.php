<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use Illuminate\Support\Facades\Http;

class SearchController extends Controller
{

    public function fetch(SearchRequest $request)
    {
        $filter = $request->only('tagged', 'order','sort');
        $filter['site'] = 'stackoverflow';
        $baseUrl = "https://api.stackexchange.com/2.3/questions?";
        $baseUrl .= http_build_query($filter);

        $response = Http::get($baseUrl);
        $questions = json_decode($response->body());
        return $questions;

    }
}
