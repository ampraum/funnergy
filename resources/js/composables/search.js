import {ref} from 'vue'
import axios from "axios";
import {useRouter} from "vue-router";

export default function useSearch() {
    const questions = ref([])
    const errors = ref('')

    const router = useRouter()

    const getQuestions = async (data) => {
        errors.value = ''

        let response = await axios.get('/api/questions', {params: data})
            .catch(function (e) {
                if (e.response.status === 422) {
                    for (const key in e.response.data.errors) {
                        errors.value += e.response.data.errors[key][0] + ' ';
                    }
                }
            });
        questions.value = response.data.items;
        await router.push({name: 'search.index'})

    }

    return {
        errors,
        questions,
        getQuestions
    }
}
