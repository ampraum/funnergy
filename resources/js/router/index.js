import {createRouter, createWebHistory} from 'vue-router'
import SearchIndex from '../components/search/SearchIndex'

const routes = [
    {
        path: '/dashboard',
        name: 'search.index',
        component: SearchIndex
    }
];


export default createRouter({
    history: createWebHistory(),
    routes
})
