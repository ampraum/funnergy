require('./bootstrap');
require('alpinejs')

import Alpine from 'alpinejs';

//window.Alpine = Alpine;
//Alpine.start();

import {createApp} from "vue";

import router from './router'

import SearchIndex from './components/search/SearchIndex'

createApp({
    components: {
        SearchIndex
    }
}).use(router).mount('#app')

